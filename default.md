**The Employee acknowledges that while working for Company, the Employee will take proper
care of all company equipment that they are entrusted with. The Employee further understands
that upon termination, the Employee will return all property of Company within 10 days
and that the property will be returned in proper working order. The Employee understands
that they may be held financially responsible for damaged property. This agreement
includes, but is not limited to, the following: (laptops, cell phones, office keys, office
keyless entry devices, other equipment). The Employee understands that failure to return
equipment will be considered theft and may lead to criminal prosecution by Company.**